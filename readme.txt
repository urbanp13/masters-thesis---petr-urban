Contents of CD/GIT repository:
/readme.txt...................the file with CD/GIT contents description
/data.........................the directory of datasets
/src..........................the directory of source codes
    /preprocessing............the directory of source codes for feature extraction
    /nn.......................the directory of source codes for neural networks
    /thesis...................the directory of \LaTeX{} source codes of the thesis
/text.........................the thesis text directory
    /thesis.pdf...............the thesis text in PDF format