# author: Petr Urban
# email: urbanp13@fit.cvut.cz
# Czech Technical University in Prague

# preprocessing and extracting truth level data from rootfile
import os
import sys

from ROOT import TCanvas, TPad, TFile, TPaveText, TGraph, TProfile
from ROOT import gBenchmark, gStyle, gROOT
from ROOT import TH1F, TH2F, TLorentzVector, TVector3, TVector2
from math import sqrt, pow, acos, radians, degrees, fabs

ERROR_COUNT = -111

####################################################################################################
# class for simple stroing leaves of Higgs branch in decay tree
class higgs_branch_data(object):
    def __init__(self):
        self.quarks = []
        self.tau_hadr_neu = []
        self.tau_lep_neu = []
        self.lep_neu = []
        self.lepton = []

    def print_data(self):
        for quark in self.quarks:
            quark.print_node("")
        for t in self.tau_hadr_neu:
            t.print_node("")
        for t in self.tau_lep_neu:
            t.print_node("")
        for t in self.lep_neu:
            t.print_node("")
        for t in self.lepton:
            t.print_node("")
####################################################################################################
# class storing attributes of one particle and information about its descendants in decay tree
class Node(object):
    def __init__(self, pt, eta, phi, e, pdgId):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.e = e
        self.pdgId = pdgId
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

    def print_node(self,level):
        print(level + str(self.pdgId) + "(" + str(self.pt) + ", " + str(self.eta) + ", " + str(self.phi) + ", " + str(self.e) + ")")
        for child in self.children:
            child.print_node(level + "____")

####################################################################################################
# recursive method counting individual particles types (leptons, quarks, neutrinos and tau-neutrinos) in descendants' leaves
    def check_children(self, data, ht):
        if(self.children):
            ll = 0
            qq = 0
            nn = 0
            tn = 0
            hadrTau = False
            for child in self.children:
                if(abs(child.pdgId) in quarks):
                    hadrTau = True
            for child in self.children:
                (l,q,n,t) = child.check_children(data, hadrTau)
                ll = ll + l
                qq = qq + q
                nn = nn + n
                tn = tn + t
            return (ll, qq, nn, tn)
        elif(abs(self.pdgId) in leptons):
            data.lepton.append(Node(self.pt, self.eta, self.phi, self.e, self.pdgId))
            return (1,0,0,0)
        elif (abs(self.pdgId) in quarks):
            data.quarks.append(Node(self.pt, self.eta, self.phi, self.e, self.pdgId))
            return (0,1,0,0)
        elif (abs(self.pdgId) in neutrinos):
            data.lep_neu.append(Node(self.pt, self.eta, self.phi, self.e, self.pdgId))
            return (0,0,1,0)
        elif (abs(self.pdgId) in tau_neu):
            if(ht):
                data.tau_hadr_neu.append(Node(self.pt, self.eta, self.phi, self.e, self.pdgId))
            else:
                data.tau_lep_neu.append(Node(self.pt, self.eta, self.phi, self.e, self.pdgId))
            return (0, 0, 0, 1)
        else:
            return(ERROR_COUNT,ERROR_COUNT,ERROR_COUNT,ERROR_COUNT)

####################################################################################################
# recursive method for invariant mass reconstruction
    def reconstruct_mass(self):
        if(self.children):
            tlv = TLorentzVector()
            first = True
            for child in self.children:
                if(first):
                    tlv = child.reconstruct_mass()
                    first = False
                else:
                    tlv = tlv + child.reconstruct_mass()
            return tlv
        else:
            tlv_c = TLorentzVector()
            tlv_c.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.e)
            return tlv_c
####################################################################################################

leptons = set([11,13])
quarks = set([1,2,3,4,111,211])
neutrinos = set([12,14])
tau_neu = set([16])

GeV = 1./1E3
Mass_W = 80385.0

path_to_data = '/eos/home-p/peurban/atlas/root_files/user.magaras.13481011._000001.output.root'

####################################################################################################



root_file = TFile.Open(path_to_data)
if root_file.IsZombie():
    print("Root file is corrupt")
tree = root_file.nominal
#tree.Print()
canvas = TCanvas("canvas", "Histogram")
#histogram declaration and adjustments
hist_truth_inv_mass_w = TH1F("W+ (truth)", "Invariant mass of W+", 60, 50.0, 110.0)
hist_truth_inv_mass_top = TH1F("top (truth)", "Invariant mass of top quark", 60, 140.0, 200.0)
hist_truth_inv_mass_w_2 = TH1F("W- (truth)", "Invariant mass of W-", 60, 50.0, 110.0)
hist_truth_inv_mass_top_2 = TH1F("anti-top (truth)", "Invariant mass of top quark 2", 60, 140.0, 200.0)
hist_truth_inv_mass_higgs = TH1F("Higgs (truth)", "Invariant mass of Higgs boson", 60, 95.0, 155.0)


hist_truth_inv_mass_w.GetYaxis().SetTitle("events [#]")
hist_truth_inv_mass_w.GetXaxis().SetTitle("invariant mass [GeV/c^2]")
hist_truth_inv_mass_top.GetYaxis().SetTitle("events [#]")
hist_truth_inv_mass_top.GetXaxis().SetTitle("invariant mass [GeV/c^2]")
hist_truth_inv_mass_w_2.GetYaxis().SetTitle("events [#]")
hist_truth_inv_mass_w_2.GetXaxis().SetTitle("invariant mass [GeV/c^2]")
hist_truth_inv_mass_top_2.GetYaxis().SetTitle("events [#]")
hist_truth_inv_mass_top_2.GetXaxis().SetTitle("invariant mass [GeV/c^2]")
hist_truth_inv_mass_higgs.GetYaxis().SetTitle("events [#]")
hist_truth_inv_mass_higgs.GetXaxis().SetTitle("invariant mass [GeV/c^2]")


#vector declaration
vec_q1 = TLorentzVector()
vec_q2 = TLorentzVector()
vec_b = TLorentzVector()
vec_b2 = TLorentzVector()
vec_lep = TLorentzVector()
vec_neu_1 = TLorentzVector()
vec_neu_2 = TLorentzVector()
vec_neu_3 = TLorentzVector()
res_tlv = TLorentzVector()
h_q1 = TLorentzVector()
h_q2 = TLorentzVector()
h_lep = TLorentzVector()
h_had_tau_neu = TLorentzVector()
h_lep_tau_neu = TLorentzVector()
h_neu = TLorentzVector()

top_w_mass = 0
antitop_w_mass = 0
top_mass = 0
antitop_mass = 0
higgs_mass = 0


stat_data = open("stat_data.txt","w+")
train = open("truth.csv","w+")

count = 0
selected_count = 0
####################################################################################################
train.write(
    "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (
    "event_number", "met_met", "met_phi",
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_q_1_mass", "higgs_q_2_mass", "higgs_q_comb_mass", "higgs_lep_mass", "higgs_mass_visible",
    "tau_mass", "antitau_mass", "tau_visible_mass", "antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"))
####################################################################################################

####################################################################################################
# MAIN CYCLE
for event in tree:
    count = count + 1
    if(count < 1):
        continue
    if (count % 10000 == 0):
        print("Event number: %i" % count)
        sys.stdout.flush()

    barcodes = []
    for i in event.m_truth_barcode:
        barcodes.append(i)
        # print(i)
    pdgId = []
    for i in event.m_truth_pdgId:
        pdgId.append(i)
        # print(i)

    childrenEv = []
    for i in event.m_truth_children:
        childrenEv.append(i)
        # print(i)


    for particle_index in range(len(pdgId)):
        truth_pdgId = pdgId[particle_index]
        truth_barcode = barcodes[particle_index]
        if (truth_pdgId == 6):  # top quark branch decay
            children = []
            size_bc = len(barcodes)
            for truth_children_of_top in childrenEv[particle_index]:
                for w1_pos_index in range(size_bc):
                    if(truth_children_of_top == barcodes[w1_pos_index]):
                        t_truth_children_pdgId = pdgId[w1_pos_index]
                        if(fabs(t_truth_children_pdgId) == 24): # W boson found
                            w1_index = w1_pos_index
                            for truth_children_of_w1 in childrenEv[w1_index]:
                                for w1_child_pos_index in range(size_bc):
                                    if(truth_children_of_w1 == barcodes[w1_child_pos_index]):
                                        w1_truth_children_pdgId = pdgId[w1_child_pos_index]
                                        if(fabs(w1_truth_children_pdgId) == 24): # virtual W boson found
                                            w2_index = w1_child_pos_index
                                            for truth_children_of_w2 in childrenEv[w2_index]:
                                                for w2_child_pos_index in range(size_bc):
                                                    if(truth_children_of_w2 == barcodes[w2_child_pos_index]):
                                                        w2_truth_children_pdgId = pdgId[w2_child_pos_index]
                                                        if(fabs(w2_truth_children_pdgId) == 1 or fabs(w2_truth_children_pdgId) == 2 or \
                                                                       fabs(w2_truth_children_pdgId) == 3 or fabs(w2_truth_children_pdgId) == 4): # quark found
                                                            children.append(w2_child_pos_index)
                                        elif(fabs(w1_truth_children_pdgId) == 1 or fabs(w1_truth_children_pdgId) == 2 or \
                                                         fabs(w1_truth_children_pdgId) == 3 or fabs(w1_truth_children_pdgId) == 4): #quark found
                                            children.append(w1_child_pos_index)
                        elif(fabs(t_truth_children_pdgId) == 5): # bottom quark found
                            b_index = w1_pos_index
                            vec_b.SetPtEtaPhiE(event.m_truth_pt[b_index], event.m_truth_eta[b_index],
                                                event.m_truth_phi[b_index], event.m_truth_e[b_index])
            if(len(children) == 2):
                vec_q1.SetPtEtaPhiE(event.m_truth_pt[children[0]], event.m_truth_eta[children[0]], event.m_truth_phi[children[0]], event.m_truth_e[children[0]])
                vec_q2.SetPtEtaPhiE(event.m_truth_pt[children[1]], event.m_truth_eta[children[1]], event.m_truth_phi[children[1]], event.m_truth_e[children[1]])
                top_w_mass = (vec_q1 + vec_q2).Mag()
                top_mass = ((vec_q1 + vec_q2) + vec_b).Mag()
        elif (truth_pdgId == -6):  # antitop branch decay
            children = []
            size_bc = len(barcodes)
            for truth_children_of_top in childrenEv[particle_index]:
                for w1_pos_index in range(size_bc):
                    if (truth_children_of_top == barcodes[w1_pos_index]):
                        t_truth_children_pdgId = pdgId[w1_pos_index]
                        if (fabs(t_truth_children_pdgId) == 24):    # W boson found
                            w1_index = w1_pos_index
                            for truth_children_of_w1 in childrenEv[w1_index]:
                                for w1_child_pos_index in range(size_bc):
                                    if (truth_children_of_w1 == barcodes[w1_child_pos_index]):
                                        w1_truth_children_pdgId = pdgId[w1_child_pos_index]
                                        if (fabs(w1_truth_children_pdgId) == 24): #virtual W boson found
                                            w2_index = w1_child_pos_index
                                            for truth_children_of_w2 in childrenEv[w2_index]:
                                                for w2_child_pos_index in range(size_bc):
                                                    if (truth_children_of_w2 == barcodes[w2_child_pos_index]):
                                                        w2_truth_children_pdgId = pdgId[w2_child_pos_index]
                                                        if (fabs(w2_truth_children_pdgId) == 11 or fabs(
                                                                w2_truth_children_pdgId) == 12 or \
                                                                        fabs(w2_truth_children_pdgId) == 13 or fabs(
                                                            w2_truth_children_pdgId) == 14): # lepton or neutrino found
                                                            children.append(w2_child_pos_index)
                                        elif (fabs(w1_truth_children_pdgId) == 11 or fabs(
                                                w1_truth_children_pdgId) == 12 or \
                                                          fabs(w1_truth_children_pdgId) == 13 or fabs(
                                            w1_truth_children_pdgId) == 14): # lepton or neutrino found
                                            children.append(w1_child_pos_index)
                        elif (fabs(t_truth_children_pdgId) == 5): # bottom quark found
                            b_index = w1_pos_index
                            vec_b2.SetPtEtaPhiE(event.m_truth_pt[b_index], event.m_truth_eta[b_index],
                                               event.m_truth_phi[b_index], event.m_truth_e[b_index])
            if (len(children) == 2):
                vec_lep.SetPtEtaPhiE(event.m_truth_pt[children[0]], event.m_truth_eta[children[0]],
                                    event.m_truth_phi[children[0]], event.m_truth_e[children[0]])
                vec_neu_1.SetPtEtaPhiE(event.m_truth_pt[children[1]], event.m_truth_eta[children[1]],
                                    event.m_truth_phi[children[1]], event.m_truth_e[children[1]])
                antitop_w_mass = (vec_lep + vec_neu_1).Mag()
                antitop_mass = ((vec_lep + vec_neu_1) + vec_b2).Mag()
        elif(truth_pdgId == 25): #Higgs branch decay
            n = Node(event.m_truth_pt[particle_index],event.m_truth_eta[particle_index],event.m_truth_phi[particle_index],event.m_truth_e[particle_index], truth_pdgId)
            is_leptonic = False
            is_hadronic = False
            has_tau = False
            size_bc = len(barcodes)
            for truth_children_of_higgs in childrenEv[particle_index]:
                for w1_pos_index in range(size_bc):
                    if (truth_children_of_higgs == barcodes[w1_pos_index]):
                        higgs_truth_children_pdgId = pdgId[w1_pos_index]
                        if (fabs(higgs_truth_children_pdgId) in quarks):
                            is_hadronic = True
                        elif (fabs(higgs_truth_children_pdgId) in leptons):
                            is_leptonic = True
                        elif (fabs(higgs_truth_children_pdgId) == 15):
                            has_tau = True
                        n.add_child(Node(event.m_truth_pt[w1_pos_index],event.m_truth_eta[w1_pos_index],event.m_truth_phi[w1_pos_index],event.m_truth_e[w1_pos_index], higgs_truth_children_pdgId))
                        if (True):
                            w1_index = w1_pos_index
                            for truth_children_of_w1 in childrenEv[w1_index]:
                                for w1_child_pos_index in range(size_bc):
                                    if (truth_children_of_w1 == barcodes[w1_child_pos_index]):
                                        w1_truth_children_pdgId = pdgId[w1_child_pos_index]
                                        if (fabs(w1_truth_children_pdgId) in quarks):
                                            is_hadronic = True
                                        elif (fabs(w1_truth_children_pdgId) in leptons):
                                            is_leptonic = True
                                        (n.children[-1]).add_child(Node(event.m_truth_pt[w1_child_pos_index],event.m_truth_eta[w1_child_pos_index],event.m_truth_phi[w1_child_pos_index],event.m_truth_e[w1_child_pos_index], w1_truth_children_pdgId))
                                        if (True):
                                            w2_index = w1_child_pos_index
                                            for truth_children_of_w2 in childrenEv[w2_index]:
                                                for w2_child_pos_index in range(size_bc):
                                                    if (truth_children_of_w2 == barcodes[w2_child_pos_index]):
                                                        w2_truth_children_pdgId = pdgId[w2_child_pos_index]
                                                        if(fabs(w2_truth_children_pdgId) in quarks):
                                                            is_hadronic = True
                                                        elif(fabs(w2_truth_children_pdgId) in leptons):
                                                            is_leptonic = True
                                                        ((n.children[-1]).children[-1]).add_child(
                                                            Node(event.m_truth_pt[w2_child_pos_index],
                                                                 event.m_truth_eta[w2_child_pos_index],
                                                                 event.m_truth_phi[w2_child_pos_index],
                                                                 event.m_truth_e[w2_child_pos_index],
                                                                 w2_truth_children_pdgId))
            if(is_hadronic and is_leptonic and has_tau):        # event belongs to (2l_SS + 1Tau_had) channel
                selected_count = selected_count + 1
                dataframe = higgs_branch_data()
                (n_lep, n_quarks, n_neutrinoes, n_tauneutrinoes) = n.check_children(dataframe, False)
                if(n_lep == 1 and n_quarks == 2 and n_neutrinoes == 1 and n_tauneutrinoes == 2):
                    res_tlv = n.reconstruct_mass()
                    higgs_mass = (res_tlv).Mag()

                    # fill histograms
                    hist_truth_inv_mass_w_2.Fill(antitop_w_mass * GeV)
                    hist_truth_inv_mass_top_2.Fill(antitop_mass * GeV)
                    hist_truth_inv_mass_w.Fill(top_w_mass * GeV)
                    hist_truth_inv_mass_top.Fill(top_mass * GeV)
                    hist_truth_inv_mass_higgs.Fill(higgs_mass * GeV)
                    h_q1.SetPtEtaPhiE(dataframe.quarks[0].pt, dataframe.quarks[0].eta, dataframe.quarks[0].phi, dataframe.quarks[0].e)
                    h_q2.SetPtEtaPhiE(dataframe.quarks[1].pt, dataframe.quarks[1].eta, dataframe.quarks[1].phi, dataframe.quarks[1].e)
                    h_lep.SetPtEtaPhiE(dataframe.lepton[0].pt, dataframe.lepton[0].eta, dataframe.lepton[0].phi, dataframe.lepton[0].e)
                    h_had_tau_neu.SetPtEtaPhiE(dataframe.tau_hadr_neu[0].pt, dataframe.tau_hadr_neu[0].eta, dataframe.tau_hadr_neu[0].phi,dataframe.tau_hadr_neu[0].e)
                    h_lep_tau_neu.SetPtEtaPhiE(dataframe.tau_lep_neu[0].pt, dataframe.tau_lep_neu[0].eta, dataframe.tau_lep_neu[0].phi, dataframe.tau_lep_neu[0].e)
                    h_neu.SetPtEtaPhiE(dataframe.lep_neu[0].pt,dataframe.lep_neu[0].eta,dataframe.lep_neu[0].phi,dataframe.lep_neu[0].e)

                    # compute angle distances between jets and leptons
                    r_w_jets = sqrt((vec_q1.Phi()-vec_q2.Phi())**2+(vec_q1.Eta()-vec_q2.Eta())**2)
                    r_w_b_jet = sqrt(((vec_q1+vec_q2).Phi()-vec_b.Phi())**2+((vec_q1+vec_q2).Eta()-vec_b.Eta())**2)
                    r_higgs_jets = sqrt((h_q1.Phi()-h_q2.Phi())**2+(h_q1.Eta()-h_q2.Eta())**2)

                    # write event features to file
                    train.write(
                        "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (
                            event.eventNumber, event.met_met, event.met_phi,
                            vec_q1.Mag(),vec_q2.Mag(), vec_b.Mag(), top_w_mass, top_mass,
                            h_q1.Mag(), h_q2.Mag(), (h_q1+h_q2).Mag(), h_lep.Mag(), (h_q1+h_q2+h_lep).Mag(),
                            (h_q1 + h_q2+h_had_tau_neu).Mag(), (h_lep+h_lep_tau_neu+h_neu).Mag(),(h_q1 + h_q2).Mag(), (h_lep).Mag(),
                            vec_lep.Mag(),vec_b2.Mag(), antitop_mass, (vec_lep+vec_b2).Mag(),
                            r_w_jets, r_w_b_jet, r_higgs_jets,
                            higgs_mass))
                
stat_data.write("Selected events " + str(selected_count))
stat_data.write("Total events " +str(count))
stat_data.write("Selected ratio " + str(1.0 * selected_count / count))


hist_truth_inv_mass_w.Draw()
canvas.SaveAs("top_w_truth.pdf")
canvas.SaveAs("top_w_truth.png")
hist_truth_inv_mass_w.SaveAs("top_w_truth.root")

hist_truth_inv_mass_top.Draw()
canvas.SaveAs("top_truth.pdf")
canvas.SaveAs("top_truth.png")
hist_truth_inv_mass_top.SaveAs("top_truth.root")

hist_truth_inv_mass_w_2.Draw()
canvas.SaveAs("antitop_w_truth.pdf")
canvas.SaveAs("antitop_w_truth.png")
hist_truth_inv_mass_w_2.SaveAs("antitop_w_truth.root")

hist_truth_inv_mass_top_2.Draw()
canvas.SaveAs("antitop_truth.pdf")
canvas.SaveAs("antitop_truth.png")
hist_truth_inv_mass_top_2.SaveAs("antitop_truth.root")

hist_truth_inv_mass_higgs.Draw()
canvas.SaveAs("higgs_truth.pdf")
canvas.SaveAs("higgs_truth.png")
hist_truth_inv_mass_higgs.SaveAs("higgs_truth.root")


stat_data.close()
train.close()
