# author: Petr Urban
# email: urbanp13@fit.cvut.cz
# Czech Technical University in Prague

# neural networks working with truth level dataset
# generating histograms, learning curves and feature importances plots
from scipy.optimize import curve_fit

SEED = 42
import time
import numpy as np
np.random.seed(SEED)
from tensorflow import set_random_seed
set_random_seed(SEED)

from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from scipy.stats import norm
import pandas as pd
from itertools import chain
####################################################################################################
####################################################################################################
EPOCHS = 100
BATCH_SIZE = 2000
CV_SPLITS = 5
VAL_SPLIT = 0.2
LEARNING_RATE = 0.01


learning_rates = [0.001,0.01,0.05,0.1]
batch_sizes = [500,1000,2000,5000]

from sklearn.base import clone

####################################################################################################
# this functions determines/computes individual feature importances by smearing the values of the processed feature
# by choosing a number from normal distribution (mu=1, sigma=0.1) and multiplying the original value by it
# then it tries to predict the target value and compares the result with results from the original unchanged dataset
# return 2D array of changes between original and smeared differences
def smear_col_feat_imp(model, X_train, y_train, X_test,y_test, random_state=42):
	model_clone = clone(model)
	model_clone.random_state = random_state
	model_clone.fit(X_train, y_train)
	y_test = [item for sublist in y_test for item in sublist]

	y_predict = model_clone.predict(X_test)
	bench_diffs = y_predict - y_test
	#print(y_test[:10], "\n", y_predict[:10], "\n", bench_diffs[:10])

	# list for storing feature importances
	importances = []
	length = np.size(y_test, 0)
	randoms = np.random.normal(1, 0.1, size=length)

	# iterating over all columns and storing feature importance (difference between benchmark and new model)
	for col in range(X_train.shape[1]):
		X_tmp = np.copy(X_test)
		#print(X_test[:, col])
		for row in range(length):
			X_tmp[row, col] *= randoms[row]
		#print(X_test[:, col])
		y_predict = model_clone.predict(X_tmp)
		diffs = y_predict - y_test
		inner_imp = diffs - bench_diffs
		#print(y_test[:10], "\n", y_predict[:10], "\n", diffs[:10])
		importances.append(inner_imp)
	return importances
####################################################################################################
# baseline model of neural network used for tuning parameters
def baseline_model(dim):
	model = Sequential()
	model.add(Dense(dim, input_dim=dim, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, activation='linear'))
	return model
####################################################################################################
# deeper model (with added layers) of neural network used for tuning parameters
def deeper_model(dim):
	model = Sequential()
	model.add(Dense(dim, input_dim=dim, kernel_initializer='normal', activation='relu'))
	model.add(Dense(int(dim/2), kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, activation='linear'))
	return model
####################################################################################################
# wider model (more neurons in individual layers) of neural network used for tuning parameters
def wider_model(dim):
	model = Sequential()
	model.add(Dense(dim*2, input_dim=dim, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, activation='linear'))
	return model
####################################################################################################
# deeper and wider model (more layers and more neurons in each layer) of neural network used for tuning parameters
def wider_and_deeper_model(dim):
	model = Sequential()
	model.add(Dense(dim*2, input_dim=dim, kernel_initializer='normal', activation='relu'))
	model.add(Dense(dim, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, activation='linear'))
	return model
####################################################################################################
# function for plotting the history of changes of training and validation losses during training of neural network (single run)
def save_figure_history(history, title, name):
	fig = plt.figure()
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title(title + '\n(MAPE)')
	plt.ylabel('error [%]')
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	# plt.show()
	fig.savefig(name)
####################################################################################################
# function for plotting the history of changes of mean training and validation losses during training of neural network (cross-validation)
def plot_mean_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	n = len(histories[0].history['loss'])
	count = len(histories)
	tr = [0] * n
	val = [0] * n
	for h in histories:
		tr = [a + b for a, b in zip(tr, h.history['loss'])]
		val = [a + b for a, b in zip(val, h.history['val_loss'])]
	tr = [x / count for x in tr]
	val = [x / count for x in val]
	plt.plot(tr)
	plt.plot(val)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ', mean)')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting the history of changes of training and validation losses during training of neural network (cross-validation)
def plot_all_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	for h in histories:
		plt.plot(h.history['loss'], color='blue', linewidth=0.5)
		plt.plot(h.history['val_loss'], color='orange', linewidth=0.5)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ')')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting histogram of invariant mass distribution
# adds information about mu, sigma and fits normal distribution curve
def plot_histogram_exact(data, bins, title, name, legend):
	fig = plt.figure()
	(mean, std) = norm.fit(data)

	bins_heights, bins_edges, _ = plt.hist(data, bins=bins, alpha=0.5, label=legend)
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]
	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	if(name != "higgs_truth.pdf"):
		popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
		x = np.linspace(xlims[0], xlims[1], bins*5)
		plt.plot(x, gaus(x, *popt), 'r', label="Gaussian fit")


	plt.xlim(xlims[0]-10, xlims[1]+10)
	plt.ylim(0, max(bins_heights) * 1.1)

	if (name == "higgs_truth.pdf"):
		plt.xlim(100, 150)
	if (name != "higgs_truth.pdf"):
		plt.annotate("Gaussian fit" "\n" r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' %(popt[1], popt[2]), xy=(0, 1), xytext=(12, -12), va='top', xycoords = 'axes fraction', textcoords = 'offset points')

	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()

####################################################################################################
# function for plotting histograms of invariant mass distribution for two datasets (predict, real)
# adds information about mu, sigma and fits normal distribution curve for first dataset (predict)
def plot_histograms(predict, real, bins, title, name):
	fig = plt.figure()

	(mean, std) = norm.fit(predict)
	bins_ar = np.arange(min(predict), max(predict))
	bins_heights, bins_edges, _ = plt.hist(predict, bins=bins_ar, alpha=0.5, label='predicted')
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in
	                range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]

	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
	x = np.linspace(xlims[0], xlims[1], bins * 5)
	plt.plot(x, gaus(x, *popt), 'r', label="Gaussian fit")


	h, c, _ = plt.hist(real, bins=bins_ar, alpha=0.5, label='real')

	plt.xlim(xlims[0] - 10, xlims[1] + 10)
	plt.ylim(0, max(bins_heights) * 1.1)

	plt.annotate("Gaussian fit" "\n" r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' %(popt[1], popt[2]), xy=(0, 1), xytext=(12, -12), va='top',
	xycoords = 'axes fraction', textcoords = 'offset points')
	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting histogram of error distribution
def plot_histogram(data, bins=20, title="Histogram of errors", name="histogram.pdf"):
	fig = plt.figure()

	(mean, std) = norm.fit(data)

	bins_heights, bins_edges, _ = plt.hist(data, bins=bins, alpha=0.5, label='errors')
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in
	                range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]
	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
	x = np.linspace(xlims[0], xlims[1], bins * 5)
	plt.plot(x, gaus(x, *popt), 'r', label="Gaussian fit")
	plt.xlim(mean - 3*std, mean + 3*std)
	plt.ylim(0, max(bins_heights) * 1.1)

	plt.annotate("Gaussian fit" "\n" r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' % (popt[1], popt[2]), xy=(0, 1), xytext=(12, -12), va='top',
	             xycoords='axes fraction', textcoords='offset points')

	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for cross-validation of model m
# returns cross-validation scores
def cv_model(model_name, m, X, y, loss, lr, bs, folder):
	if(loss == 'mean_absolute_percentage_error'):
		loss2 = 'mape'
	else:
		loss2 = loss
	name_prefix = folder + model_name + "_" + loss2 + "_" + str(lr) + "_" + str(bs) + "_"
	kfold = KFold(n_splits=CV_SPLITS, shuffle=True, random_state=SEED)
	cvscores = []
	histories = []
	adam = optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
	for train, val in kfold.split(X, y):
		m.compile(loss=loss, optimizer=adam, metrics=['mean_absolute_percentage_error'])

		tmp_his = m.fit(X[train], y[train], epochs=EPOCHS, batch_size=bs, verbose=0,
		                validation_split=VAL_SPLIT)
		histories.append(tmp_his)
		# evaluate the model
		scores = m.evaluate(X[val], y[val], verbose=0)
		print("%s: %.2f %%" % (m.metrics_names[1], scores[1]))
		cvscores.append(scores[1])
	print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))

	plot_mean_cv(histories, 'error [%]', 'MAPE', model_name, lr, bs, name_prefix+'mean.pdf')
	plot_all_cv(histories, 'error [%]', 'MAPE', model_name, lr, bs, name_prefix+'all.pdf')
	return cvscores

####################################################################################################
#function plotting feature importances
# sorts the features in descending order (bottom one is the most important)
def plot_feat_importance(imps, df, name, prefix, mean, title):
	fig = plt.figure()

	df_box = pd.DataFrame(imps, index=df.columns.tolist()[:-1])
	df_box = df_box / 1000

	means = [np.mean([el for el in sublist]) for sublist in imps]
	# df_box['means'] = means
	# df_box = df_box.sort_values(['means'], ascending=[1])
	# df_box = df_box.drop('means', 1)


	mins = [np.min(sublist) for sublist in imps]
	maxs = [np.max(sublist) for sublist in imps]
	diffs = [x1 - x2 for (x1, x2) in zip(maxs, mins)]
	df_box['diffs'] = diffs
	df_box = df_box.sort_values(['diffs'], ascending=[0])
	df_box = df_box.drop('diffs', 1)

	df_box.T.boxplot(vert=False, showmeans=False, showfliers=False, whis=[0, 100])
	plt.subplots_adjust(left=0.25)
	plt.annotate(r'$m_H=%.3f\ GeV/c^2$' % (mean / 1000), xy=(0, 1), xytext=(12, -12), va='top',
	             xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="0.8", ec="none"))
	plt.title(title)
	plt.xlabel(r'$change\ of\ invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(prefix + name + ".pdf")
	fig.clf()

####################################################################################################
# plotting histograms of individual particle invariant masses
####################################################################################################
#Plot histograms of W boson, top and antitop quarks and Higgs boson masses
df_truth_masses = pd.read_csv("../../data/truth.csv", usecols = ["top_w_mass", "top_mass",
    "antitop_mass", "higgs_mass"])
hist_values = df_truth_masses.values
hist_titles_exact = ["Invariant mass of W boson", "Invariant mass of top quark", "Invariant mass of antitop quark", "Invariant mass of Higgs boson"]
hist_filenames_exact = ["top_w_truth.pdf", "top_truth.pdf", "antitop_truth.pdf", "higgs_truth.pdf"]
hist_legends_exact = ["W boson", "top quark", "antitop quark", "Higgs boson"]

'''for i in range(3):
	plot_histogram_exact(hist_values[:,i]/1000, 60, hist_titles_exact[i], hist_filenames_exact[i], hist_legends_exact[i])'''

#plot_histogram_exact(hist_values[:,3]/1000, 60, hist_titles_exact[3], hist_filenames_exact[3], hist_legends_exact[3])

####################################################################################################
# loading inputs and splitting into train and test sets
####################################################################################################
#Load datasets
df_truth_all = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_q_1_mass", "higgs_q_2_mass", "higgs_q_comb_mass", "higgs_lep_mass", "higgs_mass_visible",
	"tau_mass","antitau_mass", "tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_truth_vis = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_q_1_mass", "higgs_q_2_mass", "higgs_q_comb_mass", "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_truth_det_available = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_truth_all_higgs_branch = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "higgs_q_1_mass", "higgs_q_2_mass", "higgs_q_comb_mass", "higgs_lep_mass", "higgs_mass_visible",
	"tau_mass","antitau_mass","tau_visible_mass","antitau_visible_mass",
    "r_higgs_jets",
    "higgs_mass"])

df_truth_vis_higgs_branch = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "higgs_q_1_mass", "higgs_q_2_mass", "higgs_q_comb_mass", "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "r_higgs_jets",
    "higgs_mass"])

df_truth_det_available_higgs_branch = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "r_higgs_jets",
    "higgs_mass"])



#selection lists
dfs = [df_truth_all, df_truth_vis, df_truth_det_available, df_truth_all_higgs_branch, df_truth_vis_higgs_branch, df_truth_det_available_higgs_branch]
hist_titles = ["full truth", "visible truth", "detector level available", "full Higgs branch", "visible Higgs branch", "detector level available Higgs branch"]
hist_filenames = ["full_truth", "vis_truth", "det_av_truth", "full_higgs_truth", "vis_higgs_truth", "det_av_higgs_truth"]

DATASETS_NUMBER = len(dfs)



####################################################################################################
####################################################################################################
# following part serves for tuning hyperparameters using cross-validation
'''model_names = ['base', 'deep', 'wide', 'deep_wide']
models = [baseline_model, deeper_model, wider_model, wider_and_deeper_model]

out = open("times.txt","w+")
err = open("errors.txt","w+")

df = dfs[0]
np_values = df.values
dimension = np.size(np_values, 1) - 1  # number of attributes, label not counted
#print (dimension)
X = np_values[:, :-1]
y = np_values[:, -1:]

for model_name,model in zip(model_names,models):
	for learning_rate in learning_rates:
		for batch_size in batch_sizes:
			start = time.time()
			cvscores = cv_model(model_name, model, X, y, 'mean_absolute_percentage_error', learning_rate, batch_size, "truth\\")
			end = time.time()
			#print(model_name + "_" + 'mape' + "_" + str(learning_rate) + "_" + str(batch_size) + " : " + str(end - start))
			out.write(model_name + "_" + 'mape' + "_" + str(learning_rate) + "_" + str(batch_size) + " : " + str(end - start))
			err.write("%s -> %.2f%% (+/- %.2f%%)\n" % (model_name + "_" + 'mape' + "_" + str(learning_rate) + "_" + str(batch_size), np.mean(cvscores), np.std(cvscores)))

out.close()
err.close()
'''


####################################################################################################
# following part serves for generating histograms and/or learning curve (train/validation loss history)
'''for i in range(DATASETS_NUMBER):
	SELECTED = i
	df = dfs[SELECTED]
	np_values = df.values
	dimension = np.size(np_values, 1) - 1  # number of attributes, label not counted
	#print (dimension)
	X = np_values[:, :-1]
	y = np_values[:, -1:]
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, shuffle=False, random_state=SEED)
	model_name = 'custom'
	model = Sequential()
	model.add(Dense(dimension * 2, input_dim=dimension, kernel_initializer='normal', activation='relu'))
	model.add(Dense(dimension, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, activation='linear'))
	adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
	model.compile(loss='mean_absolute_percentage_error', optimizer=adam, metrics=['mean_absolute_percentage_error'])
	history = model.fit(X_train, y_train, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0, validation_split=VAL_SPLIT, shuffle=False)

	#save_figure_history(history, "Truth dataset", "selected_model_truth.pdf")

	y_predict=model.predict(X_test)
	diffs = y_predict - y_test
	plot_histograms(y_predict / 1000, y_test / 1000, 60, "Comparison of histograms of invariant mass,\n" + hist_titles[SELECTED] + " dataset", "hist_predict-real_" + hist_filenames[SELECTED] + ".pdf")
	plot_histogram(diffs/1000, bins=100, title="Histogram of differences between real and predicted values,\n" + hist_titles[SELECTED] + " dataset", name="hist_diff_" + hist_filenames[SELECTED]+ ".pdf")
	scores_train = model.evaluate(X_train, y_train, verbose=0)
	scores_test = model.evaluate(X_test, y_test, verbose=0)
	print("Error: %.2f%%" % (scores_test[0]))
'''
####################################################################################################
# following part serves for visualization of feature importances
'''for i in range(DATASETS_NUMBER):
	SELECTED = i
	df = dfs[SELECTED]
	np_values = df.values
	dimension = np.size(np_values, 1) - 1  # number of attributes, label not counted
	#print(dimension)
	X = np_values[:, :-1]
	y = np_values[:, -1:]
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, shuffle=False, random_state=SEED)
	model_name = 'custom'


	def visualize_model():
		model = Sequential()
		model.add(Dense(dimension * 2, input_dim=dimension, kernel_initializer='normal', activation='relu'))
		model.add(Dense(dimension, kernel_initializer='normal', activation='relu'))
		model.add(Dense(1, activation='linear'))
		adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
		model.compile(loss='mae', optimizer=adam, metrics=['mean_absolute_percentage_error'])
		return model

	my_model = KerasRegressor(build_fn=visualize_model, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0)
	imps = smear_col_feat_imp(my_model,X_train,y_train,X_test,y_test)
	imps = [arr.tolist() for arr in imps]
	plot_feat_importance(imps, df, str(hist_filenames[SELECTED]), "feature_importance_", y_test.mean(), "Feature importance")'''

