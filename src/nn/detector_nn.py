# author: Petr Urban
# email: urbanp13@fit.cvut.cz
# Czech Technical University in Prague

# neural network trained on truth level dataset predicting the invariant mass of Higgs boson from detector data

#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset


#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset


#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset

from scipy.optimize import curve_fit

SEED = 42
import time
import numpy as np
np.random.seed(SEED)
from tensorflow import set_random_seed
set_random_seed(SEED)

from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from scipy.stats import norm
import pandas as pd
from itertools import chain
import os
####################################################################################################
####################################################################################################
EPOCHS = 100
BATCH_SIZE = 2000
CV_SPLITS = 5
VAL_SPLIT = 0.2
LEARNING_RATE = 0.01


learning_rates = [0.001,0.01,0.05,0.1]
batch_sizes = [500,1000,2000,5000]

from sklearn.base import clone

####################################################################################################
# this functions determines/computes individual feature importances by smearing the values of the processed feature
# by choosing a number from normal distribution (mu=1, sigma=0.1) and multiplying the original value by it
# then it tries to predict the target value and compares the result with results from the original unchanged dataset
# return 2D array of changes between original and smeared differences
####################################################################################################
# function for plotting the history of changes of training and validation losses during training of neural network (single run)
def save_figure_history(history, title, name):
	fig = plt.figure()
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title(title + '\n(MAPE)')
	plt.ylabel('error [%]')
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	# plt.show()
	fig.savefig(name)
####################################################################################################
# function for plotting the history of changes of mean training and validation losses during training of neural network (cross-validation)
def plot_mean_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	n = len(histories[0].history['loss'])
	count = len(histories)
	tr = [0] * n
	val = [0] * n
	for h in histories:
		tr = [a + b for a, b in zip(tr, h.history['loss'])]
		val = [a + b for a, b in zip(val, h.history['val_loss'])]
	tr = [x / count for x in tr]
	val = [x / count for x in val]
	plt.plot(tr)
	plt.plot(val)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ', mean)')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting the history of changes of training and validation losses during training of neural network (cross-validation)
def plot_all_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	for h in histories:
		plt.plot(h.history['loss'], color='blue', linewidth=0.5)
		plt.plot(h.history['val_loss'], color='orange', linewidth=0.5)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ')')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting histogram of invariant mass distribution
# adds information about mu, sigma and fits normal distribution curve
def plot_histogram_exact(data, bins, title, name, legend):
	fig = plt.figure()
	(mean, std) = norm.fit(data)

	bins_heights, bins_edges, _ = plt.hist(data, bins=bins, alpha=0.5, label=legend)
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]
	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	n = len(bins_heights)  ## <---
	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	if(name != "higgs_truth.pdf"):
		popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
		x = np.linspace(xlims[0], xlims[1], bins*2)
		plt.plot(x, gaus(x, *popt), 'r--')

	plt.xlim(xlims[0]-10, xlims[1]+10)
	plt.ylim(0, max(bins_heights) * 1.1)

	if (name == "higgs_truth.pdf"):
		plt.xlim(100, 150)
	plt.annotate(r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' %(mean, std), xy=(0, 1), xytext=(12, -12), va='top',
	xycoords = 'axes fraction', textcoords = 'offset points')

	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()

####################################################################################################
# function for plotting histograms of invariant mass distribution for two datasets (predict, real)
# adds information about mu, sigma and fits normal distribution curve for first dataset (predict)
def plot_histograms(predict, real, bins, title, name):
	fig = plt.figure()

	(mean, std) = norm.fit(predict)

	bins_heights, bins_edges, _ = plt.hist(predict, bins=bins, alpha=0.5, label='predicted')
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in
	                range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]

	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	n = len(bins_heights)  ## <---
	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
	x = np.linspace(xlims[0], xlims[1], bins * 2)
	plt.plot(x, gaus(x, *popt), 'r--')


	h, c, _ = plt.hist(real, bins=bins, alpha=0.5, label='real')

	plt.xlim(xlims[0] - 10, xlims[1] + 10)
	plt.ylim(0, max(bins_heights) * 1.1)

	plt.annotate(r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' %(mean, std), xy=(0, 1), xytext=(12, -12), va='top',
	xycoords = 'axes fraction', textcoords = 'offset points')
	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting histogram of error distribution
def plot_histogram(data, bins=20, title="Histogram of errors", name="histogram.pdf"):
	fig = plt.figure()

	(mean, std) = norm.fit(data)

	bins_heights, bins_edges, _ = plt.hist(data, bins=bins, alpha=0.5, label='errors')
	bins_centers = [0.5 * (bins_edges[r] + bins_edges[r + 1]) for r in
	                range(len(bins_edges) - 1)]  # center points of bins
	bins_heights = list(bins_heights)  # heights of bins
	xlims = [min(bins_centers), max(bins_centers)]
	angles = np.array(bins_centers)
	bins_heights = np.array(bins_heights)

	n = len(bins_heights)  ## <---
	mu = sum(bins_heights * bins_centers) / n
	sigma = np.sqrt(sum(bins_heights * (bins_centers - mu) ** 2) / n)

	def gaus(x, a, mu, sigma):
		return a * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

	p0 = [max(bins_heights), mean, std]

	popt, pcov = curve_fit(gaus, angles, bins_heights, p0=p0, maxfev=8000)
	x = np.linspace(xlims[0], xlims[1], bins * 2)
	plt.plot(x, gaus(x, *popt), 'r--')
	plt.xlim(mean - 3*std, mean + 3*std)
	plt.ylim(0, max(bins_heights) * 1.1)

	plt.annotate(r'$\mu=%.3f$' "\n" r'$\sigma=%.3f$' % (mean, std), xy=(0, 1), xytext=(12, -12), va='top',
	             xycoords='axes fraction', textcoords='offset points')

	plt.legend(loc='upper right')
	plt.title(title)
	plt.ylabel(r'$samples\ [\#]$')
	plt.xlabel(r'$invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for cross-validation of model m
# returns cross-validation scores
def cv_model(model_name, m, X, y, loss, lr, bs, folder):
	if(loss == 'mean_absolute_percentage_error'):
		loss2 = 'mape'
	else:
		loss2 = loss
	name_prefix = folder + model_name + "_" + loss2 + "_" + str(lr) + "_" + str(bs) + "_"
	kfold = KFold(n_splits=CV_SPLITS, shuffle=True, random_state=SEED)
	cvscores = []
	histories = []
	adam = optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
	for train, val in kfold.split(X, y):
		m.compile(loss=loss, optimizer=adam, metrics=['mean_absolute_percentage_error'])

		tmp_his = m.fit(X[train], y[train], epochs=EPOCHS, batch_size=bs, verbose=0,
		                validation_split=VAL_SPLIT)
		histories.append(tmp_his)
		# evaluate the model
		scores = m.evaluate(X[val], y[val], verbose=0)
		print("%s: %.2f %%" % (m.metrics_names[1], scores[1]))
		cvscores.append(scores[1])
	print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))

	plot_mean_cv(histories, 'error [%]', 'MAPE', model_name, lr, bs, name_prefix+'mean.pdf')
	plot_all_cv(histories, 'error [%]', 'MAPE', model_name, lr, bs, name_prefix+'all.pdf')
	return cvscores

####################################################################################################
#function plotting feature importances
# sorts the features in descending order (bottom one is the most important)
def plot_feat_importance(imps, df, name, prefix, mean, title):
	fig = plt.figure()

	df_box = pd.DataFrame(imps, index=df.columns.tolist()[:-1])
	df_box = df_box / 1000

	means = [np.mean([el for el in sublist]) for sublist in imps]
	# df_box['means'] = means
	# df_box = df_box.sort_values(['means'], ascending=[1])
	# df_box = df_box.drop('means', 1)


	mins = [np.min(sublist) for sublist in imps]
	maxs = [np.max(sublist) for sublist in imps]
	diffs = [x1 - x2 for (x1, x2) in zip(maxs, mins)]
	df_box['diffs'] = diffs
	df_box = df_box.sort_values(['diffs'], ascending=[0])
	df_box = df_box.drop('diffs', 1)

	df_box.T.boxplot(vert=False, showmeans=False, showfliers=False)
	plt.subplots_adjust(left=0.25)
	plt.annotate(r'$m_H=%.3f\ GeV/c^2$' % (mean / 1000), xy=(0, 1), xytext=(12, -12), va='top',
	             xycoords='axes fraction', textcoords='offset points', bbox=dict(boxstyle="round", fc="0.8", ec="none"))
	plt.title(title)
	plt.xlabel(r'$change\ of\ invariant\ mass\ [GeV/c^2]$')
	plt.show()
	fig.savefig(prefix + name + ".pdf")
	fig.clf()

####################################################################################################
# plotting histograms of individual particle invariant masses
####################################################################################################
#Plot histograms of W boson, top and antitop quarks and Higgs boson masses
'''df_truth_masses = pd.read_csv("../../data/truth.csv", usecols = ["top_w_mass", "top_mass",
    "antitop_mass_visible", "higgs_mass_visible"])
hist_values = df_truth_masses.values
hist_titles_exact = ["Visible part of the invariant mass of W boson", "Visible part of the invariant mass of top quark",
                     "Visible part of the invariant mass of antitop quark", "Visible part of the invariant mass of Higgs boson"]
hist_filenames_exact = ["top_w_truth.pdf", "top_truth.pdf", "antitop_truth.pdf", "higgs_truth.pdf"]
hist_legends_exact = ["W boson", "top quark", "antitop quark", "Higgs boson"]

for i in range(3):
	plot_histogram_exact(hist_values[:,i]/1000, 60, hist_titles_exact[i], hist_filenames_exact[i], hist_legends_exact[i])

plot_histogram_exact(hist_values[:,3]/1000, 60, hist_titles_exact[3], hist_filenames_exact[3], hist_legends_exact[3])
'''
####################################################################################################
# loading inputs and splitting into train and test sets
####################################################################################################
#Load datasets

print("ONLY SKELETON OF THE CODE, USELESS WITHOUT DETECTOR LEVEL DATASET")
df_truth = pd.read_csv("../../data/truth.csv", usecols = ["met_met", "met_phi",    #train
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_detector = pd.read_csv("../../data/detector.csv", usecols = ["met_met", "met_phi",  #test
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

#correct higgs_mass feature based on runNumber and eventNumber when detector dataset is finished

np_truth = df_truth.values
X = np_truth[:, :-1]
y = np_truth[:, -1:]
dimension = np.size(df_truth, 1) - 1  # number of attributes, label not counted

np_detector = df_detector.values
X_test = np_detector[:, :-1]
y_test = np_detector[:, -1:]

#cross-validation
'''
out = open("times.txt","w+")
err = open("errors.txt","w+")





model_name = "truth-detector"
model = Sequential()
model.add(Dense(dimension * 2, input_dim=dimension, kernel_initializer='normal', activation='relu'))
model.add(Dense(dimension, kernel_initializer='normal', activation='relu'))
model.add(Dense(1, activation='linear'))
adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
model.compile(loss='mae', optimizer=adam, metrics=['mean_absolute_percentage_error'])

start = time.time()
cvscores = cv_model(model_name, model, X, y, 'mean_absolute_percentage_error', LEARNING_RATE, BATCH_SIZE, "")
end = time.time()
out.write(model_name + "_" + 'mape' + "_" + str(LEARNING_RATE) + "_" + str(BATCH_SIZE) + " : " + str(end - start))
err.write("%s -> %.2f%% (+/- %.2f%%)\n" % (model_name + "_" + 'mape' + "_" + str(LEARNING_RATE) + "_" + str(BATCH_SIZE), np.mean(cvscores), np.std(cvscores)))

out.close()
err.close()
'''


####################################################################################################
# following part serves for generating histograms and/or learning curve (train/validation loss history)
model_name = 'custom'
model = Sequential()
model.add(Dense(dimension * 2, input_dim=dimension, kernel_initializer='normal', activation='relu'))
model.add(Dense(dimension, kernel_initializer='normal', activation='relu'))
model.add(Dense(1, activation='linear'))
adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
model.compile(loss='mean_absolute_percentage_error', optimizer=adam, metrics=['mean_absolute_percentage_error'])
history = model.fit(X, y, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0, validation_split=VAL_SPLIT, shuffle=False)

#save_figure_history(history, "Truth dataset", "selected_model_truth.pdf")

y_predict=model.predict(X_test)
diffs = y_predict - y_test
plot_histograms(y_predict / 1000, y_test / 1000, 60, "Comparison of histograms of invariant mass,\n" + "detector" + " dataset", "hist_predict-real_" + "truth-detector" + ".pdf")
plot_histogram(diffs/1000, bins=100, title="Histogram of differences between real and predicted values,\n" + "detector" + " dataset", name="hist_diff_" + "truth-detector" + ".pdf")
scores_test = model.evaluate(X_test, y_test, verbose=0)
print("Error: %.2f%%" % (scores_test[0]))

#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset


#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset


#DISCLAIMER
#This is only skeleton of the program that needs to be changed before usage on detector level dataset
