# author: Petr Urban
# email: urbanp13@fit.cvut.cz
# Czech Technical University in Prague

# neural network trained to distinguish signal events(1) from background data(0)

#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry

#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry

#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry

SEED = 42
import time
import numpy as np
np.random.seed(SEED)
from tensorflow import set_random_seed, confusion_matrix

set_random_seed(SEED)

from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
from scipy.stats import norm
import pandas as pd
from itertools import chain
import os
####################################################################################################
####################################################################################################
EPOCHS = 100
BATCH_SIZE = 2000
CV_SPLITS = 5
VAL_SPLIT = 0.2
LEARNING_RATE = 0.01

####################################################################################################
# function for plotting the history of changes of mean training and validation losses during training of neural network (cross-validation)
def plot_mean_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	n = len(histories[0].history['loss'])
	count = len(histories)
	tr = [0] * n
	val = [0] * n
	for h in histories:
		tr = [a + b for a, b in zip(tr, h.history['loss'])]
		val = [a + b for a, b in zip(val, h.history['val_loss'])]
	tr = [x / count for x in tr]
	val = [x / count for x in val]
	plt.plot(tr)
	plt.plot(val)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ', mean)')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
####################################################################################################
# function for plotting the history of changes of training and validation losses during training of neural network (cross-validation)
def plot_all_cv(histories, ylabel, loss, network, lr, bs, name):
	fig = plt.figure()
	for h in histories:
		plt.plot(h.history['loss'], color='blue', linewidth=0.5)
		plt.plot(h.history['val_loss'], color='orange', linewidth=0.5)
	plt.title('Change of error during training\n(' + loss + ', ' + network + ', ' + str(lr) + ', ' + str(bs) + ')')
	plt.ylabel(ylabel)
	plt.xlabel('epoch [#]')
	plt.legend(['train', 'validation'], loc='upper right')
	#plt.show()
	fig.savefig(name)
	fig.clf()
###################################################################################################
# function for cross-validation of model m
# returns cross-validation scores
def cv_model(model_name, m, X, y, loss, lr, bs, folder):
	name_prefix = folder + model_name + "_" + loss + "_" + str(lr) + "_" + str(bs) + "_"
	kfold = StratifiedKFold(n_splits=CV_SPLITS, shuffle=True, random_state=SEED)
	cvscores = []
	histories = []
	adam = optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
	for train, val in kfold.split(X, y):
		m.compile(loss=loss, optimizer=adam, metrics=['accuracy'])

		tmp_his = m.fit(X[train], y[train], epochs=EPOCHS, batch_size=bs, verbose=0, validation_split=VAL_SPLIT)
		histories.append(tmp_his)
		# evaluate the model
		scores = m.evaluate(X[val], y[val], verbose=0)
		print("%s: %.2f %%" % (m.metrics_names[1], scores[1]))
		cvscores.append(scores[1])
	print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))

	plot_mean_cv(histories, 'error [%]', 'accuracy', model_name, lr, bs, name_prefix+'mean.pdf')
	plot_all_cv(histories, 'error [%]', 'accuracy', model_name, lr, bs, name_prefix+'all.pdf')
	return cvscores
####################################################################################################


#Load datasets

print("ONLY SKELETON OF THE CODE, USELESS WITHOUT DETECTOR LEVEL DATASET")
df_truth_sig = pd.read_csv("../../data/truth_sig.csv", usecols = ["met_met", "met_phi",    #train
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_truth_bgr = pd.read_csv("../../data/truth_bgr.csv", usecols = ["met_met", "met_phi",    #train
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])


df_detector_sig = pd.read_csv("../../data/detector_sig.csv", usecols = ["met_met", "met_phi",  #test
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])

df_detector_bgr = pd.read_csv("../../data/detector_bgr.csv", usecols = ["met_met", "met_phi",  #test
    "top_q_1_mass", "top_q_2_mass", "top_b_mass", "top_w_mass", "top_mass",
    "higgs_lep_mass", "higgs_mass_visible",
	"tau_visible_mass","antitau_visible_mass",
    "antitop_lep_mass", "antitop_b_mass", "antitop_mass_visible",
    "r_w_jets", "r_w_b_jet","r_higgs_jets",
    "higgs_mass"])


df_truth_sig['signal'] = 1
df_truth_bgr['signal'] = 0
df_truth_comb = pd.concat([df_truth_sig, df_truth_bgr], axis=0)
df_detector_sig['signal'] = 1
df_detector_bgr['signal'] = 0
df_detector_comb = pd.concat([df_detector_sig, df_detector_bgr], axis=0)



np_truth = df_truth_comb.values
X = np_truth[:, :-1]
y = np_truth[:, -1:]
dimension = np.size(df_truth_comb, 1) - 1  # number of attributes, label not counted

np_detector = df_detector_comb.values
X_test = np_detector[:, :-1]
y_test = np_detector[:, -1:]


model_name = "signal-background distinguish"


classifier = Sequential()
classifier.add(Dense(4,input_dim=dimension, activation='relu', kernel_initializer='random_normal'))
classifier.add(Dense(4, activation='relu', kernel_initializer='random_normal'))
classifier.add(Dense(1, activation='sigmoid', kernel_initializer='random_normal'))
adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
classifier.compile(optimizer=adam,loss='binary_crossentropy', metrics =['accuracy'])

#cross-validation
out = open("times.txt","w+")
err = open("errors.txt","w+")


start = time.time()
cvscores = cv_model(model_name, classifier, X, y, 'binary_crossentropy', LEARNING_RATE, BATCH_SIZE, "")
end = time.time()
out.write(model_name + "_" + 'mape' + "_" + str(LEARNING_RATE) + "_" + str(BATCH_SIZE) + " : " + str(end - start))
err.write("%s -> %.2f (+/- %.2f)\n" % (model_name + "_" + 'accuracy' + "_" + str(LEARNING_RATE) + "_" + str(BATCH_SIZE), np.mean(cvscores), np.std(cvscores)))

out.close()
err.close()


#fit and predict
#tune parameters after having the needed datasets
classifier.fit(X, y, batch_size=BATCH_SIZE, epochs=EPOCHS)

y_predict=classifier.predict(X_test)
y_predict =(y_predict>0.5)

print(confusion_matrix(y_test, y_predict))

#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry


#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry


#DISCLAIMER
#This is only skeleton of the program that needs to be changed and respective datasets added to the data direcotry

