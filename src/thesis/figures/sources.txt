atlas_scheme : https://cds.cern.ch/images/CERN-GE-0803012-01
standard_model : https://en.wikipedia.org/wiki/Standard_Model#/media/File:Standard_Model_of_Elementary_Particles.svg
comp_neurons : https://qph.fs.quoracdn.net/main-qimg-bc7fe92df7c9be5e697169f127a2dd8e
cern_complex : https://en.wikipedia.org/wiki/CERN#/media/File:Cern-accelerator-complex.svg
conv_nn : https://cdn-images-1.medium.com/max/2400/1*vkQ0hXDaQv57sALXAJquxA.jpeg
channels : in bibtex file